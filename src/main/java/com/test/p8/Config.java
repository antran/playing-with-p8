package com.test.p8;

/**
 * @author an.tran
 */
public class Config {
    public static final String P8_URI = "http://p852:9080/wsi/FNCEWS40MTOM/";
    public static final String P8_USERNAME = "p8admin";
    public static final String P8_PASSWORD = "p8admin";
    public static final String P8_SUBJECT = "FileNetP8WSI";
    public static final String P8_JAAS_STANZA = "TARGET";

    public static final String DOCUMENT_LOCAL_PATH = "document.txt";
    public static final String DOCUMENT_LOCAL_NAME = "document.txt";
    public static final String UPDATED_DOCUMENT_LOCAL_PATH = "updated-document.txt";
    public static final String FOLDER_NAME = "p8Folder";
}
