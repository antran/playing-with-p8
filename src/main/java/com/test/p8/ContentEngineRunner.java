package com.test.p8;

import com.test.p8.runner.DocumentRunner;
import com.test.p8.runner.FolderRunner;
import org.apache.log4j.Logger;

/**
 * @author an.tran
 */
public class ContentEngineRunner {
    private static final Logger logger = Logger.getLogger(ContentEngineRunner.class);

    public static void main(String[] args) {
        logger.info("Start Content Engine Runner");
        EntryPoint entryPoint = null;
        try {
            entryPoint = new EntryPoint();
            DocumentRunner documentRunner = new DocumentRunner(entryPoint.getObjectStore());
            documentRunner.run("Test_Document_" + System.currentTimeMillis());
            FolderRunner folderRunner = new FolderRunner(entryPoint.getObjectStore());
            folderRunner.run(Config.FOLDER_NAME);
        } finally {
            if (entryPoint != null) {
                entryPoint.terminate();
            }
        }
        logger.info("Stop Content Engine Runner");
    }
}
