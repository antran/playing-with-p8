package com.test.p8;

import com.filenet.api.core.Connection;
import com.filenet.api.core.Domain;
import com.filenet.api.core.Factory;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.util.UserContext;
import org.apache.log4j.Logger;

/**
 * @author an.tran
 */
public class EntryPoint {
    private static final Logger logger = Logger.getLogger(EntryPoint.class);

    private UserContext userContext;
    private Domain domain;
    private ObjectStore objectStore;

    public EntryPoint() {
        logger.info(String.format("URI = %s", Constants.P8_URI));
        Connection conn = Factory.Connection.getConnection(Constants.P8_URI);

        userContext = UserContext.get();

        logger.info(String.format("Username = %s", Constants.P8_USERNAME));
        logger.info(String.format("Password = %s", Constants.P8_PASSWORD));
        logger.info(String.format("Subject = %s", Constants.P8_SUBJECT));
        userContext.pushSubject(
                UserContext.createSubject(conn, Constants.P8_USERNAME, Constants.P8_PASSWORD, Constants.P8_SUBJECT)
        );

        domain = Factory.Domain.getInstance(conn, null);
        objectStore = Factory.ObjectStore.fetchInstance(domain, Constants.P8_OBJECT_STORE, null);
    }

    public void terminate() {
        logger.info("Terminate processing");
        userContext.popSubject();
    }

    public UserContext getUserContext() {
        return userContext;
    }

    public void setUserContext(UserContext userContext) {
        this.userContext = userContext;
    }

    public Domain getDomain() {
        return domain;
    }

    public void setDomain(Domain domain) {
        this.domain = domain;
    }

    public ObjectStore getObjectStore() {
        return objectStore;
    }

    public void setObjectStore(ObjectStore objectStore) {
        this.objectStore = objectStore;
    }
}
