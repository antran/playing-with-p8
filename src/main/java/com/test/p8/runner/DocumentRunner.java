package com.test.p8.runner;

import com.filenet.api.collection.ContentElementList;
import com.filenet.api.constants.AutoUniqueName;
import com.filenet.api.constants.DefineSecurityParentage;
import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.*;
import com.test.p8.Config;
import org.apache.log4j.Logger;

import java.io.InputStream;

/**
 * @author an.tran
 */
public class DocumentRunner extends StepRunner {
    private static final Logger logger = Logger.getLogger(DocumentRunner.class);

    public DocumentRunner(ObjectStore objectStore) {
        super(objectStore);
    }


    @Override
    protected Document create(String input) {
        logger.info(String.format("Create document with title: %s", input));
        Document document = Factory.Document.createInstance(objectStore, null);
        document.set_ContentElements(createContentElementList(Config.DOCUMENT_LOCAL_PATH));
        document.getProperties().putValue("DocumentTitle", input);
        document.set_MimeType("text/plain");
        document.save(RefreshMode.NO_REFRESH);
        logger.info("Create document successfully");
        FolderRunner folderRunner = new FolderRunner(objectStore);
        Folder folder = folderRunner.create(Config.FOLDER_NAME);
        DynamicReferentialContainmentRelationship drcr =
                (DynamicReferentialContainmentRelationship) folder.file(document,
                        AutoUniqueName.NOT_AUTO_UNIQUE,
                        null,
                        DefineSecurityParentage.DEFINE_SECURITY_PARENTAGE);
        drcr.save(RefreshMode.NO_REFRESH);

        return document;
    }

    @Override
    protected Document retrieve(String input) {
        String path = "/" + Config.FOLDER_NAME + "/"+ input + ".txt";
        logger.info("Retrieve document: " + path);
        Document document = Factory.Document.fetchInstance(objectStore, path, null);
        logger.info("Retrieve document completed: " + document);
        return document;
    }

    @Override
    protected Document update(String input) {
        logger.info("Update document: " + input);
        Document document = retrieve(input);
        document.set_ContentElements(createContentElementList(Config.UPDATED_DOCUMENT_LOCAL_PATH));
        document.save(RefreshMode.NO_REFRESH);
        logger.info("Update document completed");
        return document;
    }

    @Override
    protected void delete(String input) {
        logger.info("Delete document: " + input);
        Document document = retrieve(input);
        document.delete();
        document.save(RefreshMode.NO_REFRESH);
        logger.info("Delete document completed");
    }

    private ContentTransfer createContentTransfer(String documentPath) {
        ContentTransfer contentTransfer = Factory.ContentTransfer.createInstance();
        InputStream inputSteam = getClass().getClassLoader().getResourceAsStream(documentPath);
        contentTransfer.setCaptureSource(inputSteam);
        contentTransfer.set_RetrievalName(Config.DOCUMENT_LOCAL_NAME);
        contentTransfer.set_ContentType("text/plain");

        return contentTransfer;
    }

    @SuppressWarnings("unchecked")
    private ContentElementList createContentElementList(String documentPath) {
        ContentElementList elementList = Factory.ContentElement.createList();
        elementList.add(createContentTransfer(documentPath));
        return elementList;
    }
}
