package com.test.p8.runner;

import com.filenet.api.constants.RefreshMode;
import com.filenet.api.core.Document;
import com.filenet.api.core.Factory;
import com.filenet.api.core.Folder;
import com.filenet.api.core.ObjectStore;
import com.filenet.api.exception.EngineRuntimeException;
import com.filenet.api.exception.ExceptionCode;
import org.apache.log4j.Logger;

/**
 * @author an.tran
 */
public class FolderRunner extends StepRunner {
    private static final Logger logger = Logger.getLogger(FolderRunner.class);

    public FolderRunner(ObjectStore objectStore) {
        super(objectStore);
    }

    @Override
    protected Folder create(String folderName) {
        logger.info("Create folder: /" + folderName);
        Folder folder = null;
        try {
             folder = Factory.Folder.createInstance(objectStore, null);
            Folder root = Factory.Folder.getInstance(objectStore, null, "/");
            folder.set_Parent(root);
            folder.set_FolderName(folderName);
            folder.save(RefreshMode.NO_REFRESH);
        } catch (EngineRuntimeException e) {
            // Ignore existing folder exception
            if (e.getExceptionCode() != ExceptionCode.E_NOT_UNIQUE) {
                throw e;
            }
            folder = Factory.Folder.getInstance(objectStore, null, "/" + folderName);
        }
        logger.info("Create folder completed");
        return folder;
    }

    @Override
    protected Folder retrieve(String folderName) {
        logger.info("Retrieve folder: /" + folderName);
        Folder folder = Factory.Folder.getInstance(objectStore, null, "/" + folderName);
        logger.info("Retrieve folder completed: " + folder);
        return folder;
    }

    @Override
    protected Folder update(String input) {
        // Skip
        return null;
    }

    @Override
    protected void delete(String folderName) {
        logger.info("Delete folder: /" + folderName);
        Folder folder = Factory.Folder.getInstance(objectStore, null, "/" + folderName);
        folder.delete();
        folder.save(RefreshMode.NO_REFRESH);
        logger.info("Delete folder completed");
    }

    public void file(Document document) {

    }
}
