package com.test.p8.runner;

import com.filenet.api.core.ObjectStore;
import com.filenet.api.core.RepositoryObject;

/**
 * @author an.tran
 */
public abstract class StepRunner {
    protected ObjectStore objectStore;

    public StepRunner(ObjectStore objectStore) {
        this.objectStore = objectStore;
    }

    public void run(String input) {
        create(input);
        retrieve(input);
        update(input);
        delete(input);
    }

    protected abstract RepositoryObject create(String input);

    protected abstract RepositoryObject retrieve(String input);

    protected abstract RepositoryObject update(String input);

    protected abstract void delete(String input);
}
